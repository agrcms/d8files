#!/bin/sh

cd `dirname $0`
mkdir -p temp && cd temp && rm -rf *
wget --user=$TEMP_USER --password=$TEMP_PASSWD https://agriculture-dev.9pro.ca/ag.sql.txt
wget --user=$TEMP_USER --password=$TEMP_PASSWD https://agriculture-dev.9pro.ca/files.tgz.txt
mv ag.sql.txt agrisource.sql
tar zcvf asource.sql.tgz agrisource.sql
tar zxvf files.tgz.txt
mv asource.sql.tgz ../proto/
rm -rf ../proto/files && mv files ../proto/
cd .. && git add .
git commit -m "refresh"
git push
