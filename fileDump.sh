#!/bin/bash

ssh -p52225 govagrisource@9pro.ca '
cd ../../web/docroot/html/
pushd sites/default
tar --exclude files/legacy -zcvf ~/files.tgz files
popd
../vendor/drush/drush/drush sql-dump --result-file=~/agrisource.sql
cd
tar zcf asource.sql.tgz ./agrisource.sql
'
cd `dirname $0`
scp -P52225 govagrisource@9pro.ca:asource.sql.tgz proto/
scp -P52225 govagrisource@9pro.ca:files.tgz .
pushd proto && rm -rf files && tar zxf ../files.tgz && popd && rm files.tgz
git add .
git commit -m "Refresh from agrisource.9pro.ca with fileDump.sh"
git push
